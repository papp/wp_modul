<form id="form">
{foreach from=$D.MODUL.SERVER.D name=SER item=SER key=kSER}
	{foreach from=$SER.MODUL.D name=MOD item=MOD key=kMOD}
		{$TYPE[ $MOD.VERSION.D[$D.MODUL.SETTING.MODUL.D[{$kMOD}].VERSION].TYPE ] = $MOD.VERSION.D[$D.MODUL.SETTING.MODUL.D[{$kMOD}].VERSION].TYPE}
	{/foreach}
{/foreach}
	<ul class="nav nav-pills">
		{foreach from=$TYPE name=T item=T key=kT}
		<li><a href="#" onclick="$('#page').load('?D[PAGE]=modullist&D[ACTION]=get_modul&D[TYPE]={$kT}'); return false;">{$kT}</a></li>
		{/foreach}
		{*<li><a href="#" onclick="$('#modulpage').load('?D[PAGE]=modullist&D[ACTION]=get_modul'); return false;">Apps</a></li>
		<li><a href="#" onclick="$('#modulpage').load('?D[PAGE]=modullist&D[ACTION]=get_modul'); return false;">Llibrary</a></li>
		*}
	</ul>
	<br>


		<div class="panel panel-default">
			<div class="panel-heading">Module</div>
			<div class="panel-body">
				<table class="table">
					<thead>
						<th>aktive</th>
						<th>Modul</th>
						<th>Type</th>
						<th>Module</th>
						<th>Beschreibung</th>
					</thead>
					<tbody>
						{foreach from=$D.MODUL.SERVER.D name=SER item=SER key=kSER}
							{foreach from=$SER.MODUL.D name=MOD item=MOD key=kMOD}
								{*if $D.TYPE == $MOD.VERSION.D[$D.MODUL.SETTING.MODUL.D[{$kMOD}].VERSION].TYPE*}
								<tr id="trSEO{$kMOD}">
									<td><input id="D[MODUL][SETTING][MODUL][D][{$kMOD}][ACTIVE]" name="D[MODUL][SETTING][MODUL][D][{$kMOD}][ACTIVE]" type="hidden" value="{$D.MODUL.SETTING.MODUL.D[{$kMOD}].ACTIVE}">
										<input type="checkbox" {if $D.MODUL.SETTING.MODUL.D[{$kMOD}].ACTIVE}checked{/if} onclick="document.getElementById('D[MODUL][SETTING][MODUL][D][{$kMOD}][ACTIVE]').value = this.checked?'1':'0'">
									</td>
									<td>{$kMOD}</td>
									<td>{$MOD.TYPE}</td>
									<td>
										{foreach from=$MOD.MODUL name=MO1 item=MO1 key=kMO1}
											{if $MOD.MODUL[$kMO1]|count > 0}
												<b>{$kMO1}</b>
												<ul class="list-group">
												{foreach from=$MOD.MODUL[$kMO1] name=MO item=MO key=kMO}
													{if $kMO}<li class="list-group-item {if ($kMO|array_key_exists:$SER.MODUL.D AND $kMO1 != 'INCOMPATIBLE') || (!$kMO|array_key_exists:$SER.MODUL.D AND $kMO1 == 'INCOMPATIBLE')}list-group-item-success{elseif $kMO1 == 'REQUIRED' || $kMO1 == 'INCOMPATIBLE'}list-group-item-danger{elseif $kMO1 == 'OPTIONAL'}list-group-item-warning{/if}"><span class="badge">{$MO.VERSION}</span>{$kMO}</li>{/if}
												{/foreach}
												</ul>
											{/if}
										{/foreach}
									</td>
									<td><b>{$MOD.TITLE}</b><br>{$MOD.DESCRIPTION}</td>
									
								</tr>
								{*/if*}
							{/foreach}
						{/foreach}
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-right">
				<div class="btn-group">
					<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=modullist&D[ACTION]=set_modul', data : $('#form').serialize() /*, success: function(data) { $('#page').load('?D[PAGE]=seourl');}*/ });" class="btn btn-default">Speichern</button>
					<button type="button" onclick="$('#page').load('?D[PAGE]=modullist'); return false;" class="btn btn-default">Abbrechen</button>
				</div>
			</div>
		</div>
</form>