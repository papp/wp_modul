<form id="form">
	<ul class="nav nav-pills">
		<li><a href="#" onclick="$('#modulpage').load('?D[PAGE]=modullist&D[ACTION]=get_store'); return false;">Store</a></li>
		<li><a href="#" onclick="$('#modulpage').load('?D[PAGE]=modullist&D[ACTION]=get_modul'); return false;">Installiert <span class="badge">{$D.MODUL.D|count}</span></a></li>
	</ul>
	<br>
	<div id='modulpage'></div>
	{*
	<div id='store'>
	
		<div class="input-group" style="margin:auto; max-width:400px;">
			<input type="text" class="form-control" placeholder="Search for...">
			<span class="input-group-btn">
				<button class="btn btn-default" type="button">Suchen</button>
			</span>
		</div>
		<br>
		
		<div class="row">
			{foreach from=$D.MODUL.D name=MOD item=MOD key=kMOD}
			<div class="col-sm-6 col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">{$MOD.TITLE}</div>
					<div class="panel-body" style="height:200px;">
						<img src="{$D.MODUL.SERVER.D[$MOD.SERVER_ID].URL}{$MOD.IMAGE}" >
							{$MOD.TYPE} - {$MOD.VERSION}V<br>

							{$MOD.DESCRIPTION|truncate:100:"...":true}
							
						
					</div>
					<div class="panel-footer">
						<a href="#" class="btn btn-primary" role="button">Install</a>
					</div>
				</div>
			</div>
			{/foreach}
		</div>
		
	</div>
	
	<div id='local'>  
		<div class="panel panel-default">
			<div class="panel-heading">Module</div>
			<div class="panel-body">
				<table class="table">
					<thead>
						<th>aktive</th>
						<th>Modul</th>
						<th>Version</th>
						<th>Type</th>
						<th>Beschreibung</th>
					</thead>
					<tbody>
						{foreach from=$D.MODUL.SERVER.D name=SER item=SER key=kSER}
							{foreach from=$SER.MODUL.D name=MOD item=MOD key=kMOD}
							<tr id="trSEO{$kMOD}">
								<td><input id="D[MODUL][SERVER][D][{$kSER}][MODUL][D][{$kMOD}][ACTIVE]" name="D[MODUL][SERVER][D][{$kSER}][MODUL][D][{$kMOD}][ACTIVE]" type="hidden" value="{$MOD.ACTIVE}">
									<input type="checkbox" {if $MOD.ACTIVE}checked{/if} onclick="document.getElementById('D[MODUL][SERVER][D][{$kSER}][MODUL][D][{$kMOD}][ACTIVE]').value = this.checked?'1':'0'">
								</td>
								<td>{$kMOD}</td>
								<td>
									<select name="sD[MODUL][SERVER][D][{$kSER}][MODUL][D][{$kMOD}][VERSION][D]">
									{foreach from=$MOD.VERSION.D name=VER item=VER key=kVER}
										<option {if $VER.ACTIVE}selected{/if}>{$kVER}</option>
									{/foreach}
									</select>
								</td>
								<td>{$MOD.TYPE}</td>
								<td>{$MOD.DESCRIPTION}</td>
							</tr>
							{/foreach}
						{/foreach}
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-right">
				<div class="btn-group">
					<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=modullist&D[ACTION]=set_modul', data : $('#form').serialize() /*, success: function(data) { $('#page').load('?D[PAGE]=seourl');}*/ });" class="btn btn-default">Speichern</button>
					<button type="button" onclick="$('#page').load('?D[PAGE]=modullist'); return false;" class="btn btn-default">Abbrechen</button>
				</div>
			</div>
		</div>
		
	</div>*}
</form>