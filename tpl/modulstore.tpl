<form id="form">
		<div class="input-group" style="margin:auto; max-width:400px;">
			<input type="text" class="form-control" placeholder="Search for...">
			<span class="input-group-btn">
				<button class="btn btn-default" type="button">Suchen</button>
			</span>
		</div>
		<br>
		
		<div class="row">
			{foreach from=$D.MODUL.SERVER.D name=SER item=SER key=kSER}
				{foreach from=$SER.MODUL.D name=MOD item=MOD key=kMOD}
				<div class="col-sm-4 col-md-4 col-lg-3">
					<div class="panel panel-default">
						<div class="panel-heading">{$MOD.TITLE}</div>
						<div class="panel-body" style="height:200px;">
							<img src="{$D.MODUL.SERVER.D[$MOD.SERVER_ID].URL}{$MOD.IMAGE}" ><br>
							Typ: {$MOD.TYPE} | Version: {$MOD.VERSION.D}<br>
							Server: {$kSER}<br>
							{$MOD.DESCRIPTION|truncate:100:"...":true}

						</div>
						<div class="panel-footer">
							<a href="#" class="btn btn-primary" role="button">Install</a>
						</div>
					</div>
				</div>
				{/foreach}
			{/foreach}
		</div>
</form>