<?
class wp_modul__modullist extends wp_modul__modullist__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'set_modul':
				$this->C->modul()->set_modul();
				exit;
				break;
			case 'get_modul':
				$this->C->modul()->get_modul();
				$this->C->library()->smarty()->assign('D', $this->D);
				$this->C->library()->smarty()->display(__dir__.'/tpl/modullocal.tpl');
				exit;
				break;
			case 'get_store':
				$this->C->modul()->get_store();
				$this->C->library()->smarty()->assign('D', $this->D);
				$this->C->library()->smarty()->display(__dir__.'/tpl/modulstore.tpl');
				exit;
				break;
		}
		

		#$this->C->seo()->get_url();
		#$this->C->library()->smarty()->assign('D', $this->D);
		#$this->C->library()->smarty()->display(__dir__.'/tpl/modullist.tpl');
	}
	
	function show()
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/modullist.tpl');
	}
}