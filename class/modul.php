<?
class wp_modul__class__modul extends wp_modul__class__modul__parent
{
	function __construct(&$D = null)
	{
		parent::{__function__}($D);
    }

	function get_store()
	{
		$data = $this->get_data(['DB_NAME' => 'modul_store']);
		#Lade alle SERVER
		if($data['SERVER']['D'] && date('YmdHis') > $data['SETTING']['CACHE']['TIMESTAMP'] + $data['SETTING']['CACHE']['CACHE_TIME'] )
		{
			foreach((array)$data['SERVER']['D'] AS $kS => $vS)
			{
				$SET = $vS;
				$SET['URL'] = "localhost:8080/test/script";#ToDo:
				$SET['SSL'] = 0;
				$con = file_get_contents( (($SET['SSL'])?'https://':'http://'). $SET['URL'].'?D[PAGE]=modulpublic');
				$s = json_decode($con,1);
				$data['SERVER']['D'][ $SET['URL'] ] = $this->D['MODUL']['SERVER']['D'][ $SET['URL'] ] = $s['SERVER']['D'][ 'localhost' ];
			}
			$data['SETTING']['CACHE']['TIMESTAMP'] = date('YmdHis');
			$this->set_data($data);
		}
	}
	
	function get_modul()
	{
		$W_ID = $this->D['MODUL']['W']['ID'];
		
		$this->D['MODUL'] = $this->get_data([ 'DB_NAME' => 'modul_local' ]); #lade SETTING
		$M = dir('system');
		while (false !== ($entry1 = $M->read()))
		{
			if($entry1 != '.' && $entry1 != '..' && ($W_ID == '' || $W_ID == $entry1 ) )
			{					
				$file = "system/{$entry1}/modul.json";
				if(file_exists($file))
				{
					$arr = file_get_contents($file);
					$this->D['MODUL']['SERVER']['D']['localhost']['MODUL']['D'][$entry1] = json_decode($arr,1);
				}	
			}
		}
	}
	
	function set_modul()
	{
		#Modul Update, Aktive & Inaktive, delete
		$data = $this->get_data([ 'DB_NAME' => 'modul_local' ]);
		$this->_remove("tmp/system/");
		#echo "<pre>";print_r($this->D['MODUL']['SETTING']['MODUL']['D']);exit;
		foreach((array)$this->D['MODUL']['SETTING']['MODUL']['D'] AS $kM => $vM)
		{
			$MODUL = $this->D['MODUL']['SETTING']['MODUL']['D'][ $kM ];
			if($MODUL['ACTIVE'])
			{
				#1.copy library
				if(file_exists("system/{$kM}/library"))
					$this->_copy("system/{$kM}/library/", "tmp/system/library/");
				
				#1.1. copy tpl
				if(file_exists("system/{$kM}/tpl"))
					$this->_copy("system/{$kM}/tpl/", "tmp/system/tpl/");
				
				#2.crate class
				$mod = $this->_dir(['PATH' => "system/{$kM}/"]);
				foreach((array)$mod['FILE'] AS $kF => $vF)
					if($mod['FILE'][ $kF ]['EXTENSION'] == 'php')
						$D1['FILE'][ $mod['FILE'][ $kF ]['FILENAME'] ]['MODUL'][ $kM ] = $MODUL;
				
				#3.crate page class
				$mod = $this->_dir(['PATH' => "system/{$kM}/class/"]);
				foreach((array)$mod['FILE'] AS $kF => $vF)
					if($mod['FILE'][ $kF ]['EXTENSION'] == 'php')
						$D2['FILE'][ $mod['FILE'][ $kF ]['FILENAME'] ]['MODUL'][ $kM ] = $MODUL;
			}
		}
		#echo "<pre>";
		$this->_getclassmerge($D1);
		$D2['TO'] = 'class';
		$this->_getclassmerge($D2);
		#print_R($D2);exit;
		#exit;
		#ist stand laden
		#$data = $this->get_data();
		#erstelle tmp class
		#soll stand
		$this->set_data([ 'DB_NAME' => 'modul_local', 'SETTING' => $this->D['MODUL']['SETTING'] ]);
	}
	
	/**
	* $ModID	ID des Moduls, entspricht dem Modul Ordner Namen
	* $D		Array weitere Parameter optional D[URL] = SERVER_URL(EigenServer,GitLab) wen null = local
	* return	array
	*/
	function getModulInfo($ModID,$D=null)
	{
		if($D['URL'])
		{
			$arr = file_get_contents("{$D['URL']}{$ModID}/raw/master/modul.json");
		}
		else
		{
			#$arr = file_get_contents("system/{$ModID}/modul.json");
		}
		return json_decode($arr,1);
	}
	
	#Installiert und update alle Module neu wenn Modul inactive, so wird dieser nicht mit installiert
	function InstallModuls()
	{
		#lade modul.db und prüfe welche Updates bis wan ausgeführt wurden und  ob Modul aktive ist.
		#for Modul
		#Prüfe ob Updates ausgeführt werden müssen
		#installiere Module
	}
	
	/*
	function installModul($ModID, $ID=null)
	{
		#ToDo: führe Install.php von 100-n aus.
		$M = dir("system/{$ModID}");
		while (false !== ($entry1 = $M->read()))
		{
			if($entry1 != '.' && $entry1 != '..' )
			{
				$file = "system/{$ModID}/{$entry1}/modul.json";
				if(file_exists($file))
				{
					$arr = file_get_contents($file);
					#ToDo: Prüfe ob Version keine dev ist
					if(is_file("system/{$ModID}/{$entry1}/install.php"))
					{
						include "system/{$ModID}/{$entry1}/install.php";
						$CLASS = "{$ModID}_install";
						$CLASS::install();
					}
				}
			}
		}
	}
	*/
	
	#Download Reqursive Modul mit abhängigen Modulen
	#ModID, D[URL] = SERVER_URL(EigenServer,GitLab)
	function rModulDownload($ModID, $D=null)
	{
		$Mod = $this->getModulInfo($ModID,$D);
		$this->ModulDownload($ModID,$D);
			foreach($Mod['MODUL']['REQUIRED'] as $k => $v )
			{
				if(!is_file("tmp/download/{$k}.zip"))
				{
					$this->rModulDownload($k,$D);
				}
			}
	}
	
	#Lade ein Modul herunter
	#ModID, D[URL] = SERVER_URL(EigenServer,GitLab)
	function ModulDownload($ModID, $D=null)
	{
		if(!is_dir('tmp/download'))
			mkdir('tmp/download',0777,1);
		copy("{$D['URL']}{$ModID}/repository/archive.zip?ref=master","tmp/download/{$ModID}.zip");
	}
	
	function unzipModulDownload($ModID, $D=null)
	{
		$this->_remove("system/{$ModID}");
		$this->_unzip("tmp/download/{$ModID}.zip", "system/{$ModID}/");
		$d = $this->_dir2("system/{$ModID}/");
		$this->_copy("system/{$ModID}/{$d[0]}/","system/{$ModID}/");
		$this->_remove("system/{$ModID}/{$d[0]}");#ToDo: Kontainer Ordner wird nicht gelöscht!
		unlink("tmp/download/{$ModID}.zip");
	}
	
	/*
	#ModulID, D= ['URL' = 'SERVER|null(local)','RECURSIVE' = '1|0']
	function downloadModul($mID, $D=null)
	{
		$Mod = $this->getModulInfo($ModID,$D);
		$Mod_Ver = $Mod['MODUL']['D'][$ModID]['VERSION']['LAST_STABLE'];
		
		if(!is_dir('tmp/download'))
			mkdir('tmp/download',0777,1);
		if( strpos($D['URL'], 'gitlab.com') !== false)
			copy("{$D['URL']}{$ModID}/repository/archive.zip?ref=master","tmp/download/{$ModID}.zip");
		else
			copy("{$D['URL']}?D[PAGE]=modulpublic&D[MODUL][W][ID]={$ModID}&D[ACTION]=download","tmp/download/{$ModID}.zip");
		
		if($Mod['MODUL']['D'][$ModID]['VERSION']['D'][$Mod_Ver]['MODUL']['REQUIRED'] && $D['RECURSIVE'])
			foreach($Mod['MODUL']['D'][$ModID]['VERSION']['D'][$Mod_Ver]['MODUL']['REQUIRED'] as $k => $v )
			{
				if(!is_file("tmp/download/{$k}.zip"))
				{
					$this->downloadModul($k,$D);
				}
			}
	}
	*/
	#Speichert Einstellungen für Module
	function set_data($D)
	{
		$file = "data/{$D['DB_NAME']}.db";
		file_put_contents($file,json_encode($D) );
	}
	
	#Laden Einstellungen für Module
	function get_data($D=null)
	{
		$file = "data/{$D['DB_NAME']}.db";
		if(file_exists($file))
			$cnf = file_get_contents($file);
		return json_decode($cnf,1);
	}
	
	function _getclassmerge($D)
	{	
		
		foreach((array)$D['FILE'] AS $kF => $vF)
		{
			$DATE = date('Y.m.d H:i:s');
			$FILE = $kF;
			$R .= "<?\n#===CLASS {$FILE} START {$DATE} ===\n";
			$m = 0;
			foreach((array)$D['FILE'][ $kF ]['MODUL'] AS $kM => $vM)
			{
				$MODUL = $kM;
				$Ex = ($D['TO'])? "__{$D['TO']}":'';
				if($m == 0)#Anfang
				{
					$R .= 'class '."{$MODUL}{$Ex}__{$FILE}".'__parent { function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; } function __call($m, $a){ return $a[0]; } }'."\n";
					$R .= "include('system/{$MODUL}/{$D['TO']}/{$FILE}.php');\n";
					$_PRE_MODUL = $MODUL;
					$m++;
				}
				else #Mitte
				{
					$R .= "class {$MODUL}{$Ex}__{$FILE}__parent extends {$_PRE_MODUL}{$Ex}__{$FILE}{ function __construct(){ parent::{__function__}(); } }\n";
					$R .= "include('system/{$MODUL}/{$D['TO']}/{$FILE}.php');\n";
					$_PRE_MODUL = $MODUL;
				}
				
			}
			$R .= "class {$FILE} extends {$MODUL}{$Ex}__{$FILE} { function __construct(){ parent::{__function__}(); } }\n";
		
			$R .= "#===CLASS {$FILE} END =========================";
			
			$this->_mkdir("tmp/system/{$D['TO']}/");
			$this->_write("tmp/system/{$D['TO']}/{$FILE}.php",$R);
			$R = '';
		}
		
		return $R;
	}
	
	
	#=======CFile CLASS==========
	function _dir($D)
	{
		if (is_dir($D['PATH']))
		{
			if ($dh = opendir($D['PATH']))
			{
				while (($file = readdir($dh)) !== false)
				{
					if($file != '.' && $file != '..')
						if(!is_file($D['PATH'] . $file)) #filetype($D['PATH'] . $file) == 'dir')
						{
							$FILE = $this->_dir(['PATH' => $D['PATH'] . $file.'/']);
							$D['DIR'][ $file ] = array(
								'NAME'	=>	$file,
								'FILE'	=>	$FILE['FILE'],
								);
						}
						else
						{
							$pi = pathinfo($file);
							$fi = stat($D['PATH'].$file);
							#Nur bei jpeg, && Tiff
							if(strtolower($pi['extension']) == 'jpg' || strtolower($pi['extension']) == 'jpeg' || $pi['extension'] == 'tiff')
							{
								$rd = exif_read_data($D['PATH'].$file, 0, false);
								
								if($rd['DateTimeOriginal'])
									$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeOriginal']);
								else if($rd['DateTimeDigitized'])
									$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeDigitized']);
								else if($rd['DateTime'])
									$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTime']);  
							}
							
							$D['FILE'][ $file ] = array(
								'NAME'			=> $file,
								'FILENAME'		=> $pi['filename'],
								'EXTENSION'		=> $pi['extension'],
								'SIZE'			=> $fi['size'], #filesize($D['PATH'].$file),
								'CREATE_TIME'	=> date('YmdHis',$fi['ctime']),
								'EDIT_TIME'		=> date('YmdHis',$fi['mtime']),
								'RECORDING_TIME'=> $recording_time,#aufnahme Datum
								);
						}
					
				}
				closedir($dh);
			}
		}
		return $D;
	}
	function _dir2($dir)
	{
		$d = dir($dir);
		while (false !== ($entry = $d->read()))
			if($entry != '..' && $entry != '.')
				$DIR[] = $entry;
		$d->close();
		return $DIR;
	}
	function _copy($from, $to, $D = null)
	{
		$D['CHMOD'] = (!$D['CHMOD'])?0777:$D['CHMOD'];
		#Erstelle Verzeichnis fals nicht exsistiert
		$pi = pathinfo($to);
		$this->_mkdir($pi['dirname'], $D);
		
		if(is_array($from) && isset($from['tmp_name'])) #Ist Upload
		{
			for($i=0; $i < count($from['tmp_name']); $i++)
				move_uploaded_file($from['tmp_name'][$i], $to.'/'.$from['name'][$i]);
		}
		elseif(is_file($from)) #File
		{
			if(strpos($from,'http://') !== false )
				$from = str_replace(' ','%20',$from);
			copy($from,$to);
		}
		elseif(is_dir($from)) #Dir
		{
			$dh = opendir($from);
			while (($file = readdir($dh)) !== false)
			{
				if($file != '.' && $file != '..')
					if(is_file($from.$file))
						$this->_copy($from.$file,$to.$file,$D);
					else
						$this->_copy($from.$file.'/',$to.$file.'/',$D);
			}
		}
	}
		
	function _mkdir($pfad, $D=null)
	{
		$D['CHMOD'] = (!$D['CHMOD'])?0777:$D['CHMOD'];
		$D['RECURSIVE'] = (!$D['RECURSIVE'])?true:$D['RECURSIVE'];
		$oldumask = umask(0);
		@mkdir($pfad, $D['CHMOD'], $D['RECURSIVE']);
		umask($oldumask);
	}
		
	function _remove($from)
	{ 
		if(substr($from, -1, 1) == '/')
		$from .= '*';

		foreach (glob($from) as $filename)
		{
			if(!is_file($filename))
			{
				$this->_remove("{$filename}/*");
				if(!@rmdir($filename))
				{
					chmod($filename, 0777);
					rmdir($filename);
				}
			}
			else
				unlink($filename);
		}
	}
	
	function _write($url,$text,$modus=null)
	{
		$$modus = ($modus == 'APPEND')?'FILE_APPEND':null;
		file_put_contents($url,$text, $modus);
	}
	
	function _unzip($file_zip, $to_dir)
	{
		$zip = new ZipArchive;
		$res = $zip->open($file_zip);
		if ($res === TRUE) {
		  $zip->extractTo($to_dir);
		  $zip->close();
		} else {
		  echo 'doh!';
		}
	}
}